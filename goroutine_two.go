package main

import (
	"fmt"
	"time"
)

func GoroutineTwo() {
	var channelOne chan string = make(chan string)
	var channelTwo chan string = make(chan string)

	go func() {
		for {
			channelOne <- "From channel one."
			time.Sleep(time.Second * 2)
		}
	}()

	go func() {
		for {
			channelTwo <- "From channel two."
			time.Sleep(time.Second * 3)
		}
	}()

	go func() {
		for {
			select {
			case messageOne := <-channelOne:
				fmt.Println(messageOne)
			case messageTwo := <-channelTwo:
				fmt.Println(messageTwo)
			case timeout := <-time.After(time.Second):
				fmt.Println("[", timeout, "] No messages ready.")
			}
		}
	}()

	// Wait for input so the program doesn't close
	// abruptly after it finished running the program.
	var input string
	fmt.Scanln(&input)
}

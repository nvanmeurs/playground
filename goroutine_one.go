package main

import (
	"fmt"
	"time"
)

func image(c chan<- string) {
	// Send a message to the channel repeatedly.
	for {
		// Send a message to the channel.
		c <- "An image"
	}
}

func textFile(c chan<- string) {
	// Send a message to the channel repeatedly.
	for {
		// Send a message to the channel.
		c <- "A textfile"
	}
}

func printer(c <-chan string) {
	// Print messages retrieved from the channel repeatedly.
	for {
		// Retrieve a message from the channel.
		var message string = <-c

		// Print the message to the screen.
		fmt.Println(message)

		// ... wait a second!
		time.Sleep(time.Second * 1)
	}
}

func GoRoutineOne() {
	// Channel used to communicate between goroutines.
	var mychannel chan string = make(chan string)

	// Execute the messager and printer functions in their
	// own goroutines.
	go image(mychannel)
	go textFile(mychannel)
	go printer(mychannel)

	// Wait for input so the program doesn't close
	// abruptly after it finished running the program.
	var input string
	fmt.Scanln(&input)
}

package main

import (
	"fmt"
)

func CliExample() {
	fmt.Println("Please input a number: ")
	var input float64
	fmt.Scanf("%f", &input)

	var output float64 = input * 2

	fmt.Println(output)
}

package main

import (
	"fmt"
)

func factorial(x uint64) uint64 {
	if x == 0 {
		return 1
	}

	return x * factorial(x-1)
}

func fibo(n int) int {
	if n < 2 {
		return n
	}
	return fibo(n-2) + fibo(n-1)
}

func Recursion() {
	for i := 1; i <= 20; i++ {
		fmt.Println(fibo(i))
	}
}

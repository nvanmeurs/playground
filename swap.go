package main

import (
	"fmt"
)

func swapNums(x, y *int64) {
	*x, *y = *y, *x
}

func Swap() {
	var x, y int64 = 1, 2
	fmt.Println(x, y)
	swapNums(&x, &y)
	fmt.Println(x, y)
}

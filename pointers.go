package main

import (
	"fmt"
)

func zero(xPtr *int64) {
	*xPtr = 0
}

func Pointers() {
	var x int64 = 35
	fmt.Println(x)
	zero(&x)
	fmt.Println(x)
}

package main

import (
	"fmt"
)

func FahrenheitToCelcius(degrees float64) {
	var celsius float64 = (degrees - 32) * 5 / 9
	fmt.Println(celsius)
}
